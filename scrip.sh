#!/bin/bash
clear 
clear 

rm compilador ejercicios1/pruebas/compilador
make clean
make all
cp compilador ejercicios1/pruebas/compilador
cd ejercicios1/pruebas/
rm *.out

for fichero in *.in 
do
		echo		
		echo -e "\e[32m******************SALIDA EJERCICIO  $fichero...******************"
		echo -e "\e[39m*"
		echo "*"
		echo "*"
			 ./compilador $fichero $fichero.out
		echo "*"
		echo "*"
		echo -e "\e[31m*^^^^^^^^^^^^^^^^^^^^^^^NASM^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
		echo -e "\e[39m"	            
      			nasm -f elf $fichero.out
			gcc -Wall -o bin/$fichero $fichero.o alfalib.o
		echo -e "\e[31m*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
	        echo -e "\e[39m"	
		echo
		echo -e "\e[32m************************HECHO  $fichero***************************" 		
		echo -e "\e[39m"
		
done
cd ..
