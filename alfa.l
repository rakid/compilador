%{
#include "alfa.h"
#include <stdio.h>
#include <string.h>
#include "y.tab.h"

int l=1, c=1; //liena y columna
int flag=0;
%}


%option noyywrap

SEPARADORES 			[ \t]
COMENTARIO  			"//".*\n
DIGITO 				[0-9]
LETRAS 				[a-zA-Z]+
NUMERO 				[0-9]+
IDENTIFICADOR 	 		{LETRAS}+({NUMERO}|{LETRAS})*

%%

"main" 		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_MAIN	;}                 
"int" 		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_INT     ;}            
"boolean"	{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_BOOLEAN ;}            
"array"		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_ARRAY   ;}            
"function"	{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_FUNCTION;}            
"if"		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_IF      ;}            
"else"		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_ELSE    ;}            
"while"		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_WHILE   ;}            
"scanf"		{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_SCANF   ;}            
"printf"	{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_PRINTF  ;}            
"return"	{c+=yyleng;
				fprintf(yyout,";D:\t%s\n",yytext);
				return TOK_RETURN  ;}            



";" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_PUNTOYCOMA         ;} 
"," 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_COMA               ;} 
"(" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_PARENTESISIZQUIERDO;} 
")" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_PARENTESISDERECHO  ;} 
"[" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_CORCHETEIZQUIERDO  ;} 
"]" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_CORCHETEDERECHO    ;} 
"{" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_LLAVEIZQUIERDA     ;} 
"}" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_LLAVEDERECHA       ;} 
"=" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_ASIGNACION         ;} 
"+" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MAS                ;} 
"-" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MENOS              ;} 
"/" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_DIVISION           ;} 
"*" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_ASTERISCO          ;} 
"&&" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_AND                ;} 
"||" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_OR                 ;} 
"!" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_NOT                ;} 
"==" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_IGUAL              ;} 
"!=" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_DISTINTO           ;} 
"<=" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MENORIGUAL         ;} 
">=" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MAYORIGUAL         ;} 
"<" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MENOR              ;} 
">" 	{c+=yyleng;fprintf(yyout,";D:\t%s\n",yytext); return TOK_MAYOR              ;} 

"true"			{
					c+=yyleng;
					fprintf(yyout,";D:\t%s\n",yytext);
					
					yylval.atributos.es_direccion 	= 0;
    				yylval.atributos.tipo 			= BOOLEAN;
    				yylval.atributos.valor_entero 			= 1;
					
					return TOK_TRUE;
				}    
"false"			{
					c+=yyleng;
					fprintf(yyout,";D:\t%s\n",yytext);
						
					yylval.atributos.es_direccion	= 0;
    				yylval.atributos.tipo 			= BOOLEAN;
    				yylval.atributos.valor_entero 			= 0;
					
					return TOK_FALSE;
				}    

"\r" {}
"\n" {	l++;c=1;}

{SEPARADORES} 	{c+=yyleng;}
{COMENTARIO}	{l++;c=1;}
{NUMERO}		{
					c+=yyleng;
					fprintf(yyout,";D\t%s\n",yytext); 
					
					yylval.atributos.tipo			= INT;
					yylval.atributos.es_direccion	= 0;
					yylval.atributos.valor_entero 	= atoi(yytext);
					
					return TOK_CONSTANTE_ENTERA;
				}    

{IDENTIFICADOR} {
					if(yyleng >100){ 
						fprintf(yyout,"****Error en [lin %d, col %d]: identificador demasiado largo (%s)\n",l,c,yytext);
						fprintf(yyout,"TOK_ERROR	-1	%s\n",yytext);
						flag=1;
						c+=yyleng;
						return TOK_ERROR;
					}else{
						fprintf(yyout,";D:\t%s\n",yytext);
						strcpy(yylval.atributos.lexema, yytext);  
						yylval.atributos.tipo			= INT;
						yylval.atributos.es_direccion	= 0;
						yylval.atributos.valor_entero 	= 0;
	  					
						c+=yyleng;
						return TOK_IDENTIFICADOR;
					}
				}



. {fprintf(stdout,"****Error en [lin %d, col %d]: simbolo no permitido (%s)\n",l,c,yytext);fprintf(stdout,"TOK_ERROR	-1	%s\n",yytext);c+=yyleng;return TOK_ERROR;}

%%
