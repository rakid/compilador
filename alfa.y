%{
#include <stdio.h>
#include <string.h>
#include "generadorCodigo.h"

extern int l;
extern int c;
extern char *yytext;
extern int yylex();
extern int flag;
extern FILE *yyout;

tabla_hash tGlobal;
tabla_hash tLocal;
tabla_hash *tActual;

elemento_tabla e;
elemento_tabla *e_aux;

int local;
int yyerror(char* cadena);
int tipo_actual;
int clase_actual;
int tamanio_vector_actual;
int num_variables_locales_actual;
int pos_variable_local_actual;
int num_parametros_actual;
int pos_parametros_actual;
int ambito_actual = GLOBAL_AREA;
int etiqueta;

char error_msg [1000];

%}

%union 
{
	tipo_atributos atributos;
}

%token <atributos> TOK_IDENTIFICADOR
%token <atributos> TOK_CONSTANTE_ENTERA

%type <atributos> programa
%type <atributos> declaraciones
%type <atributos> clase
%type <atributos> tipo
%type <atributos> clase_vector
%type <atributos> identificadores
%type <atributos> identificador
%type <atributos> funciones
%type <atributos> funcion
%type <atributos> clase_escalar
%type <atributos> parametros_funcion
%type <atributos> resto_parametros_funcion
%type <atributos> sentencias
%type <atributos> sentencia
%type <atributos> sentencia_simple 
%type <atributos> bloque
%type <atributos> asignacion
%type <atributos> elemento_vector
%type <atributos> condicional
%type <atributos> bucle
%type <atributos> lectura
%type <atributos> escritura
%type <atributos> retorno_funcion
%type <atributos> lista_expresiones
%type <atributos> exp
%type <atributos> comparacion
%type <atributos> constante_entera
%type <atributos> constante_logica
%type <atributos> constante

%type <atributos> fn_name

%type <atributos> fn_declaration
%type <atributos> if_exp_sentencia
%type <atributos> while
%type <atributos> if_exp
%type <atributos> while_exp


%token TOK_MAIN

%token TOK_INT
%token TOK_BOOLEAN
%token TOK_ARRAY

%token TOK_FUNCTION

%token TOK_IF
%token TOK_ELSE
%token TOK_WHILE

%token TOK_SCANF
%token TOK_PRINTF
%token TOK_RETURN

%token TOK_PUNTOYCOMA
%token TOK_COMA
%token TOK_PARENTESISIZQUIERDO
%token TOK_PARENTESISDERECHO
%token TOK_CORCHETEIZQUIERDO
%token TOK_CORCHETEDERECHO
%token TOK_LLAVEIZQUIERDA
%token TOK_LLAVEDERECHA

%token TOK_MAS
%token TOK_MENOS
%token TOK_DIVISION
%token TOK_ASTERISCO

%token TOK_ASIGNACION

%token TOK_AND
%token TOK_OR
%token TOK_NOT

%token TOK_IGUAL
%token TOK_DISTINTO
%token TOK_MENORIGUAL
%token TOK_MAYORIGUAL
%token TOK_MENOR
%token TOK_MAYOR


%token TOK_TRUE
%token TOK_FALSE

%token TOK_ERROR

%left TOK_MAS TOK_MENOS TOK_OR 

%left TOK_DIVISION TOK_ASTERISCO TOK_AND

%right TOK_NOT MENOSU







%%

programa 				: inicioTabla TOK_MAIN  TOK_LLAVEIZQUIERDA declaraciones escritura_TS funciones sentencias TOK_LLAVEDERECHA 
							{
								fprintf(yyout,";R1:\t<programa> ::= main { <declaraciones> escritura_TS <funciones> <sentencias> }");
								escribeFin(yyout);
							};

inicioTabla				:	{
								e_aux = (elemento_tabla*)malloc(sizeof(elemento_tabla*));
	  							if (crear_tabla (&tGlobal, MAX_TAM) == -1) {
        						    yyerror("Error al crear la tabla de simbolos");
        						}
        						fprintf(yyout, "segment .bss\n");
							}
escritura_TS            : 	{ 
								escribeCabeceras(yyout);
								
                          	};
declaraciones			: declaracion   					 	{fprintf(yyout,";R2:\t<declaraciones> ::= <declaracion>\n");} 
						| declaracion declaraciones 			{fprintf(yyout,";R3:\t<declaraciones> ::= <declaracion> <declaraciones>\n");};

declaracion 			: clase identificadores TOK_PUNTOYCOMA	{
																	fprintf(yyout,";R4:\t<declaracion> ::= <clase> <identificadores> ;\n");
																};

clase 					: clase_escalar 						{
																	clase_actual = ESCALAR;
																	fprintf(yyout,";R5:\t<clase> ::= <clase_escalar>\n");
																} 
						| clase_vector 							{
																	clase_actual = VECTOR;
																	fprintf(yyout,";R7:\t<clase> ::= <clase_vector>\n");
																};

clase_escalar 			: tipo									{fprintf(yyout,";R9:\t<clase_escalar> ::= <tipo>\n");};

tipo					: TOK_INT  								{
																	tipo_actual = INT;
																	fprintf(yyout,";R10:\t<tipo> ::= int\n");
																}

						| TOK_BOOLEAN               			{
																	tipo_actual = BOOLEAN;
																	fprintf(yyout,";R11:\t<tipo> ::= bool\n");
																};

clase_vector			: TOK_ARRAY tipo TOK_CORCHETEIZQUIERDO constante TOK_CORCHETEDERECHO
																{
																	tamanio_vector_actual = $4.valor_entero;
																	if((tamanio_vector_actual<1)||(tamanio_vector_actual>MAX_TAMANIO_VECTOR)){
																		printf("ERROR SEMANTICO linea: %d, columna: %d\n",l,c);	
																	}
																	fprintf(yyout,";R15:\t<clase_vector> ::= TOK_ARRAY <tipo> [ <constante_entera> ]\n");
																};	

identificadores			: identificador  						{
																	fprintf(yyout,";R18:\t<identificadores> ::= <identificador>\n");
																}
						| TOK_IDENTIFICADOR TOK_COMA identificadores{fprintf(yyout, ";R19:	<identificadores> ::= <identificador> , <identificadores> \n");};

funciones				: funcion funciones 					{fprintf(yyout,";R20:\t<funciones> ::= <funcion> <funciones>\n");} 
						|   									{fprintf(yyout,";R21:\t<funciones> ::= \n");};

fn_name					: TOK_FUNCTION tipo TOK_IDENTIFICADOR 	{
																	if(buscar_lexema($3.lexema,&tGlobal)==0){
						                                    		    yyerror("Funcion ya declarada");
						                                    			return ;
						                                    		}
						                                    		else{

						                                    			strcpy($$.lexema, $3.lexema);
						                                    			strcpy(e.nombre, $3.lexema);
						                                    			
						                                    			num_variables_locales_actual = 0;
																		pos_variable_local_actual	 = 1;
																		num_parametros_actual		 = 0;
																		pos_parametros_actual		 = 0;
						                                    			
																		
																		e.identificador = calcula_id($3.lexema);
																		e.categoria 	= 0;
																		e.clase 		= clase_actual;
																		e.tipo 			= tipo_actual;

																		
																		e.numVarLocal   = 0;
																		e.posVarLocal 	= pos_variable_local_actual;
																		e.numParams 	= 0;
																		e.posParam 		= 0;
																		
																		insertar(&e,&tGlobal); 

																		if (crear_tabla (&tGlobal, MAX_TAM) == -1) {
        						    										yyerror("Error al crear la tabla de simbolos");
        																}

						                                    			ambito_actual=FUNCION_AREA;
																		insertar(&e,&tLocal);

						                                    		}
																}	
fn_declaration			: fn_name TOK_PARENTESISIZQUIERDO parametros_funcion TOK_PARENTESISDERECHO TOK_LLAVEIZQUIERDA declaraciones_funcion											
																{

																	dame_elemento_lexema($1.lexema,&tLocal, e_aux);

																	e_aux->numParams 	= num_parametros_actual;
																	
																	dame_elemento_lexema($1.lexema,&tGlobal, e_aux);

																	e_aux->numParams 	= num_parametros_actual;
																	
																	strcpy($$.lexema, $1.lexema);
						                                    		
																}
funcion 				: fn_declaration sentencias TOK_LLAVEDERECHA
											 					{
											 						borra_tabla(&tLocal);

											 						ambito_actual = GLOBAL_AREA;

											 						dame_elemento_lexema($1.lexema,&tGlobal, e_aux);

																	e_aux->numParams 	= num_parametros_actual;
																	
											 						fprintf(yyout,";R22:\t<funcion> ::= TOK_FUNCTION <tipo> <identificador> ( <parametros_funcion> ) [ <declaraciones_funcion> <sentencias> ]\n");


											 					};

parametros_funcion  	: parametro_funcion resto_parametros_funcion
																{fprintf(yyout, ";R23:\t<parametros_funcion> ::= <parametro_funcion> <resto_parametros_funcion>\n");}; 
						|										{fprintf(yyout, ";R24:\t<parametros_funcion> ::= \n");}

resto_parametros_funcion: TOK_PUNTOYCOMA parametro_funcion resto_parametros_funcion
																{fprintf(yyout, ";R25:\t<resto_parametros_funcion> ::= ; <parametro_funcion> <resto_parametros_funcion>\n");} 
						| 										{fprintf(yyout, ";R26:\t<resto_parametros_funcion> ::= \n");}; 

parametro_funcion 		: tipo idpf 					{fprintf(yyout, ";R27:\t<parametro_funcion> ::= <tipo> <identificador>\n");};

idpf					: TOK_IDENTIFICADOR { 

												/*codigo Nuevo*/
													if(ambito_actual==FUNCION_AREA){
													if(buscar_lexema($1.lexema,&tLocal)==0){
														printf("ERROR SEMANTICO No existe el identificador en el ambito local");
													}
												

													pos_parametros_actual++;
													num_parametros_actual++;
													/**********************************/
						  					  }
						  					};

declaraciones_funcion	: declaraciones 			     		{fprintf(yyout, ";R28:\t<declaraciones_funcion> ::= <declaraciones>\n");}
						|										{fprintf(yyout, ";R29:\t<declaraciones_funcion> ::= \n");}

sentencias 				: sentencia 						 	{fprintf(yyout, ";R30:\t<sentencias> ::= <sentencia>\n");} 
						| sentencia sentencias {fprintf(yyout, ";R31:	<sentencias> ::= <sentencia> <sentencias>\n");};

sentencia 				: sentencia_simple TOK_PUNTOYCOMA		{fprintf(yyout, ";R32:\t<sentencia> ::= <sentencia_simple> ;\n");} 
						| bloque 								{fprintf(yyout, ";R33:	<sentencia> ::= <bloque>\n");};
sentencia_simple 		: asignacion 							{fprintf(yyout, ";R34:\t<sentencia_simple> ::= <asignacion>\n");} 
						| lectura 								{fprintf(yyout, ";R35:	<sentencia_simple> ::= <lectura>\n");} 
						| escritura 							{fprintf(yyout, ";R36:	<sentencia_simple> ::= <escritura>\n");} 
						| retorno_funcion						{fprintf(yyout, ";R38:	<sentencia_simple> ::= <retorno_funcion>\n");};
bloque					: condicional 							{fprintf(yyout, ";R40:\t<bloque> ::= <condicional>\n");} 
						| bucle 								{fprintf(yyout, ";R41:	<bloque> ::= <bucle>\n");} ;
asignacion 				: TOK_IDENTIFICADOR TOK_ASIGNACION exp 		{/*hay que ampliar los ambitos a tabla local a la 												 hora de las asignaciones*/
						                                       
						                                              if($1.tipo != $3.tipo){
									                                	sprintf(error_msg, "Asignacion incompatible");
										                                printf("****Error semantico en lin %d: %s\n", l, error_msg);
						                                    			exit(0);
						                                    		  }
						                                    		  if(buscar_lexema($1.lexema,&tGlobal)!=0){
						                                    		    $1.tipo=$3.tipo;
						                                    		  }
						                                    		  fprintf(yyout, "\tpop dword eax\n");
						                                        	  if($3.es_direccion == 1){
						                                        			fprintf(yyout, "\tmov dword eax, [eax]\n");
						                                        	  }
								                                      fprintf(yyout, "\tmov dword [_%s], eax\n", $1.lexema);
																	fprintf(yyout, ";R43:\t<asignacion> ::= <identificador> = <exp>\n");
															   }
						| elemento_vector TOK_ASIGNACION exp 	{
																  if($1.tipo!=$3.tipo){

																	printf("TIPO ERROR SEMANTICO linea: %d, columna: %d %d %d\n",l,c, $1.tipo, $3.tipo);	
																  }
																  if($3.valor_entero> MAX_TAMANIO_VECTOR||$3.valor_entero<0){
																  	printf("ERROR se excede del tamaño del vector %d\n",$3.valor_entero);
																  }
																  fprintf(yyout, "pop dword eax\n");
																  if($3.es_direccion==1){
																  	fprintf(yyout, "mov eax, [eax]\n");
																  }

																  fprintf(yyout, "pop dword edx\n");
																  fprintf(yyout, "mov [edx],eax\n");

																  fprintf(yyout, ";R44:\t<asignacion> ::= <elemento_vector> = <exp>\n");} ;
elemento_vector   		: TOK_IDENTIFICADOR TOK_CORCHETEIZQUIERDO exp TOK_CORCHETEDERECHO
																{
																
																	if(buscar_lexema($1.lexema, &tGlobal)!=0){
																		yyerror("Error no existe identificador ");
																	}

																	dame_elemento_lexema($1.lexema, &tGlobal,e_aux);
																	strcpy($$.lexema, $1.lexema);
																	printf("TIPO ELEMENTO %d NOMBRE %s\n",e_aux->tipo, e_aux->nombre);
																	$$.tipo= e_aux->tipo;	

																	gc_inicia_elemento_vector(yyout,$3.es_direccion,tamanio_vector_actual,$1.lexema);
																	fprintf(yyout, ";R48:\t<elemento_vector> ::= <identificador> [ <exp> ]\n");};
condicional				: if_exp TOK_PARENTESISDERECHO TOK_LLAVEIZQUIERDA sentencias TOK_LLAVEDERECHA 
																{fprintf(yyout, ";R50:\t<condicional> ::= TOK_IF ( <exp> ) { <sentencias> } \n");}
						| if_exp if_exp_sentencia TOK_ELSE TOK_LLAVEIZQUIERDA sentencias TOK_LLAVEDERECHA  
																{fprintf(yyout, ";R51:\t<condicional> ::= TOK_IF ( <exp> ) { <sentencias> } TOK_ELSE { <sentencias> } \n");};
if_exp					: TOK_IF TOK_PARENTESISIZQUIERDO exp 		{
																		if($3.tipo!=BOOLEAN){
																			printf("ERROR tipo de la exp no es de tipo BOOLEAN en la condicion\n");
																		}
																		$$.etiqueta=etiqueta++;
																		//gc_condicional(yyout,$$.etiqueta,$3.es_direccion);
																	}
						| TOK_IF TOK_PARENTESISIZQUIERDO exp TOK_PARENTESISDERECHO TOK_LLAVEIZQUIERDA
																    {
																    	if($3.tipo!=BOOLEAN){
																    		printf("ERROR tipo de la exp no es de tipo BOOLEAN en la condicion\n");

																    	}
																    	$$.etiqueta=etiqueta++;
																    	gc_condicional(yyout,$$.etiqueta,$3.es_direccion);
																	}											
if_exp_sentencia		:  sentencias TOK_LLAVEDERECHA				{
																		$$.etiqueta=$1.etiqueta;
																		gc_condSentencia(yyout);
																	}
bucle 					: while_exp sentencias TOK_LLAVEDERECHA  
																{fprintf(yyout, ";R52:\t<bucle> ::= TOK_WHILE ( <exp> ) { <sentencias> } \n");};
while 					: TOK_WHILE TOK_PARENTESISIZQUIERDO 	{
																	$$.etiqueta=etiqueta++;
																	gc_inicioBucle(yyout);
																}

while_exp				: while exp TOK_PARENTESISDERECHO TOK_LLAVEIZQUIERDA {
																				if($2.tipo!=BOOLEAN){
																					printf("Error semantico en lin : %d Bucle con condicion de tipo int.\n",l);
																				}
																				$$.etiqueta=$1.etiqueta;
																				gc_bucleExp(yyout,$1.es_direccion);
																			 }

lectura					: TOK_SCANF TOK_IDENTIFICADOR			{
																	if(buscar_lexema($2.lexema,&tGlobal)==-1 && clase_actual!=ESCALAR){
																 		yyerror("Error TOK_SCANF identificador");
																 	}
																	fprintf(yyout, ";R54:\t<lectura> ::= scanf <identificador>\n");
																};
escritura				: TOK_PRINTF exp						{															
																	if(buscar_lexema($2.lexema,&tGlobal)==-1 && clase_actual!=ESCALAR){
																 		yyerror("Error TOK_PRINTF exp	");
																 	}
																	gc_imprime(yyout, $2.es_direccion, $2.tipo);
																	fprintf(yyout, ";R56:\t<escritura> ::= printf <exp>\n");
																};
retorno_funcion			: TOK_RETURN exp 						{fprintf(yyout, ";R61:\t<retorno_funcion> ::= TOK_RETURN <exp>\n");};

exp 					:   exp TOK_MAS exp 					{
																 if($1.tipo!=INT||$3.tipo!=INT){
																  	 yyerror("Error exp TOK_MAS exp");

																 }else{
	                                                                 gc_suma_enteros(yyout,$1.es_direccion,$3.es_direccion);
	                                                                 $$.tipo=INT;
	                                                                 $$.es_direccion=0;
	                                                                 fprintf(yyout,";R72:\t<exp> ::= <exp> + <exp>\n")		;
                                                                 }
																}
						|	exp TOK_MENOS exp 					{
																 if($1.tipo!=INT||$3.tipo!=INT){
																 	 yyerror("Error exp TOK_MENOS exp");

																 }else{
							                                         gc_resta_enteros(yyout,$1.es_direccion,$3.es_direccion);
							                                         $$.tipo=INT;
	                                                                 $$.es_direccion=0;
	                                                                 fprintf(yyout,";R73:\t<exp> ::= <exp> - <exp>\n");
																 }
																}
						|	exp TOK_DIVISION exp 				{
																	if($1.tipo!=INT||$3.tipo!=INT){	
																		yyerror("Error exp TOK_DIVISION exp ");

																	}else{
							                                         gc_div_enteros(yyout,$1.es_direccion,$3.es_direccion);
							                                         $$.tipo=INT;
	                                                                 $$.es_direccion=0;
																		fprintf(yyout,";R74:\t<exp> ::= <exp> / exp>\n");
																	}
																}
						| 	exp TOK_ASTERISCO exp  				{
																	if($1.tipo!=INT||$3.tipo!=INT){	
																	 yyerror("Error exp TOK_ASTERISCO exp ");

																	 }else{
								                                         gc_mul_enteros(yyout,$1.es_direccion,$3.es_direccion);
								                                         $$.tipo=INT;
		                                                                 $$.es_direccion=0;
																		 fprintf(yyout,";R75:\t<exp> ::= <exp> * <exp>\n");
																	}
																}
						|	TOK_MENOS exp  %prec MENOSU 		{
						                                         	gc_not(yyout,$2.es_direccion);
						                                         	fprintf(yyout,";R76:\t<exp> ::= - exp \n");
						                                        }
						| 	exp TOK_AND exp  					{
																	if($1.tipo!=BOOLEAN||$3.tipo!=BOOLEAN){
																 		yyerror("Error exp TOK_AND exp");

																 	}else
																 	{
						                                         		gc_and(yyout,$1.es_direccion,$3.es_direccion);
						                                         		$$.tipo=BOOLEAN;
                                                                 		$$.es_direccion=0;
						                                         		fprintf(yyout,";R77:\t<exp> ::= <exp> TOK_AND <exp>\n");
						                                         	}
						                                        }
						|   exp TOK_OR exp 						{
																	if($1.tipo!=BOOLEAN||$3.tipo!=BOOLEAN)
																	{
																 		yyerror("Error  exp TOK_OR exp 	");

																	 }
																	 else
																	 {
						                                           gc_or(yyout,$1.es_direccion,$3.es_direccion);
						                                           $$.tipo=BOOLEAN;
                                                                   $$.es_direccion=0;
						                                           fprintf(yyout,";R78:\t<exp> ::= <exp> TOK_OR <exp>\n")		;}}
						| 	TOK_NOT exp 					     {
						                                         	gc_neg_logico(yyout,$2.es_direccion);
						                                         	fprintf(yyout,";R79:\t<exp> ::= TOK_NOT exp\n");
						                                         }
						|   TOK_IDENTIFICADOR					{    
                                                                    printf("ENTRO EN IDENTIFICADOR EXP %s\n",$1.lexema);
																	strcpy($$.lexema,$1.lexema);
																	$$.tipo			=$1.tipo;
																	$$.es_direccion	=$1.es_direccion;
																	fprintf(yyout,";R80:\t<exp> ::= <identificador>\n");
                        									         fprintf(yyout,";Variable\n");
                        									         fprintf(yyout,"push dword [_%s]\n", $1.lexema);
																  	//Para vectores
																  	//fprintf(f, "mov [_%s], %d\n", $1.lexema, $1.valor);
                        									  	}
						| 	constante 							{
																	$$.tipo=$1.tipo;
																	$$.es_direccion=$1.es_direccion;
																	$$.valor_entero=$1.valor_entero;
																	fprintf(yyout,";R81:\t<exp> ::= <constante>\n");
																}
						|   TOK_PARENTESISIZQUIERDO exp TOK_PARENTESISDERECHO  
																{	
																	$$.tipo=$2.tipo;
																	$$.es_direccion=$2.es_direccion;
																	fprintf(yyout,";R82:\t<exp> ::= ( exp )\n");}
						|	TOK_PARENTESISIZQUIERDO comparacion  TOK_PARENTESISDERECHO 
																{
																	$$.tipo=$2.tipo;
																	$$.es_direccion=$2.es_direccion;
																 	fprintf(yyout,";R83:\t<exp> ::= ( comparacion  )\n");}
						|	elemento_vector						{
																	$$.tipo=$1.tipo;
																 	$$.es_direccion=$1.es_direccion;
																	fprintf(yyout,";R85:\t<exp> ::= elemento_vector\n");}
						|   identificador TOK_PARENTESISIZQUIERDO lista_expresiones TOK_PARENTESISIZQUIERDO 
																{fprintf(yyout,";R88:\t<exp> ::= <identificador> ( <Lista_expresiones> )\n");};

lista_expresiones		: exp resto_lista_expresiones 			 {fprintf(yyout,";R89:\t<lista_expresiones> 	  ::= <resto_lista_expresiones>\n");}
						|  							  			 {fprintf(yyout,";R90:\t<lista_expresiones> ::= \n");};		 
resto_lista_expresiones : TOK_COMA exp resto_lista_expresiones   {fprintf(yyout,";R91:\t<resto_lista_expresiones>::= , <exp> <resto_lista_expresiones>\n");}
  						|  										 {fprintf(yyout,";R92:\t<resto_lista_expresiones> ::= \n");};
comparacion				: exp TOK_IGUAL exp  					 {
									      						  if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error  exp TOK_IGUAL exp  ");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
	                                                                  gc_comparacion_igual(yyout, $1.es_direccion, $3.es_direccion);
	                                                                  fprintf(yyout,";R93:\t<comparacion> ::= <exp> == 	 <exp>\n");}}
						| exp TOK_DISTINTO exp  				 {
																 if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error exp TOK_DISTINTO exp  ");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
							                                          gc_comparacion_distinto(yyout,  $1.es_direccion, $3.es_direccion);
							                                          fprintf(yyout,";R94:\t<comparacion> ::= <exp> !=	 <exp>\n");}}
						| exp TOK_MENORIGUAL exp  				 {
																  if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error exp TOK_MENORIGUAL exp ");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
							                                          gc_comparacion_menorIgual(yyout, $1.es_direccion, $3.es_direccion);
							                                          fprintf(yyout,";R95:\t<comparacion> ::= <exp> <= <exp>\n");}}
						| exp TOK_MAYORIGUAL exp  				 {
																  if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error exp TOK_MAYORIGUAL exp  ");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
							                                          gc_comparacion_mayorIgual(yyout, $1.es_direccion, $3.es_direccion);
							                                          fprintf(yyout,";R96:\t<comparacion> ::= <exp> >= <exp>\n");}}
						| exp TOK_MENOR exp 					 {
															      if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error exp TOK_MENOR exp");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
							                                          gc_comparacion_menor(yyout, $1.es_direccion, $3.es_direccion);
							                                          fprintf(yyout,";R97:\t<comparacion> ::= <exp> < 	 <exp>\n");}}	
						| exp TOK_MAYOR exp 	      			 {
															      if($1.tipo!=INT||$3.tipo!=INT){
																	  yyerror("Error exp TOK_MAYOR exp ");
																  } else{
																  	  $$.tipo=BOOLEAN;
																  	  $$.es_direccion=0;
							                                          gc_comparacion_mayor(yyout, $1.es_direccion, $3.es_direccion);  
							                                          fprintf(yyout,";R98:\t<comparacion> ::= <exp> > 	 <exp>\n");}};

constante 				: constante_logica						 {
																	$$.tipo=BOOLEAN;	
																	fprintf(yyout,";R99:\t<constante>::= <constante_logica>\n");
																}
						| constante_entera 						{
																	$$.tipo=INT;
																	$$.valor_entero=$1.valor_entero;
																	fprintf(yyout,";R100:\t<constante>::= <constante_entera>\n");
																};			

constante_logica		: TOK_TRUE 								 {/*analisis semantico*/
                                                                
                                                                  $$.es_direccion=0;
                                                                  /*generador de codigo*/
                                                                  fprintf(yyout,"; numero_linea %d\n", l);
                                                                  fprintf(yyout,"push dword 1\n");
                                                                  fprintf(yyout,";R102:\t<constante_logica>::= TOK_TRUE\n");
                                                                  }
						| TOK_FALSE 							 {

                                                                  $$.es_direccion=0;
                                                                  /*generador de codigo*/
                                                                  fprintf(yyout,"; numero_linea %d\n", l);
                                                                  fprintf(yyout,"push dword 1\n");
						                                          fprintf(yyout,";R103:\t<constante_logica>::= TOK_FALSE\n");};			
constante_entera        : TOK_CONSTANTE_ENTERA					 {
                                                                  
                                                                	$$.es_direccion=0;
																	$$.valor_entero=$1.valor_entero;
																	 /*generador de codigo*/
                                                                  fprintf(yyout,"; numero_linea %d\n", l);
                                                                  fprintf(yyout,"push dword %d\n",$1.valor_entero);
                                                                  fprintf(yyout,";R104:\t<constante_entera>::= TOK_CONSTANTE_ENTERA\n");};

identificador 			: TOK_IDENTIFICADOR 					{
                                                                    int tam;
																	if(ambito_actual==FUNCION_AREA){
																		tActual = &tLocal;
																	}
																	else
																	{
																		tActual = &tGlobal;
																	}
																	printf("%s\n",$1.lexema);
																	if(buscar_lexema($1.lexema, tActual)==0){
																		yyerror("SE ENCUENTRA ELEMENTO EN LA TABLA DE SIMOLOS ");
																	}
																	else
																	{
																		if (clase_actual == ESCALAR){
																			tam			= 1;
																			e.tamanio	= 0; 
	                        											}
	                        											else {
																			tam 		= tamanio_vector_actual;
																			e.tamanio	= tamanio_vector_actual;
	                        											}
																		strcpy(e.nombre, $1.lexema);
																		e.identificador = calcula_id($1.lexema);
																		e.categoria 	= 0;
																		e.clase 		= clase_actual;
																		e.tipo 			= tipo_actual;
								
																		
																		printf("nombre %s tipo %d \n\n", e.nombre, e.tipo);

																		e.numVarLocal   = 0;
																		e.posVarLocal 	= pos_variable_local_actual;
																		e.numParams 	= 0;
																		e.posParam 		= 0;
																		insertar(&e,tActual);
																		fprintf(yyout, "_%s resd %d\n", $1.lexema, tam);
																		fprintf(yyout,";R108:\t<identificador> ::= TOK_IDENTIFICADOR\n");
																	}
																};



%%

int yyerror (char* s)
{
	if (flag==0){
		//fprintf (stderr, "\nERROR SINTACTICO: \n");
		fprintf (stderr, "ERROR SINTACTICO: %d:%d %s\n", l, c, s);
		return -1;
	}
	return 0;
}
