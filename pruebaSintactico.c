#include <stdio.h>
#include <stdlib.h>


extern FILE * yyin;
extern FILE * yyout;
int yyparse();


int main(int argc, char* argv[]) {

	

    if (argc != 3) 
    {
        printf("Error en entrada de argumentos\n");
        return -1;
    }
    
    yyin = fopen(argv[1], "r");   
    if (!(yyin)) {
        return -1;
    }
    yyout = fopen(argv[2], "w");
    
    if (!yyout) {
        return -1;
    }

    // while(yylex() != 0);
yyparse();

	fclose(yyout);

	fclose(yyin);

	return 0;

}

